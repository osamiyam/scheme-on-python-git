#################################################
# Title: Scheme on Python
# Author: Osami Yamamoto (osami@meijo-u.ac.jp)
# Date: Mon Dec 10 00:00:57 JST 2018
#################################################
import re
import sys
import io
import math
import random
import traceback
import types
import time
import calendar
from tkinter import *
from fractions import Fraction

sys.setrecursionlimit(50000)

try:
    import readline
except ImportError:
    pass

prompt = ">> "
cur = ""
mode = "interactive"
filept = None
eval_orig = eval

def getLine():
    global filept, mode, prompt
    if mode == "interactive":
        return input(prompt)
    else:
        ll = filept.readline()
        if ll == "":
            filept.close()
            raise EOFError
        else:
            ll = ll.strip()
            ## print "[%s]" % ll
            return ll
        
def getToken():
    global cur
    ident_pat = r'[ \t]*(' + \
                r'[a-zA-Z<>\-\+\*/_\.!#\?=]' + \
                r'[a-zA-Z<>\-\+\*/_\.!#\?=0-9]*)'
    def get_match(m):
        global cur
        s = m.group(1)
        ll =len(m.group(0))
        ## print "ll = ", ll, "cur = ", cur
        cur = cur[ll:]
        return s
    while len(cur) == 0:
        cur = getLine()
        if re.match(r'[ \t]*;.*$', cur):
            cur = ""
    m = re.match(r'[ \t]*(\(|\)|\'|\`|\,\@|\,|\.)', cur)
    if m: return get_match(m)
    m = re.match(r'[ \t]*(\-?[0-9]+/[0-9]+)', cur)
    if m: return get_match(m)
    m = re.match(r'[ \t]*(\-?[0-9]+(\.[0-9]*)?([eE][+-]?[0-9]+)?)', cur)
    if m: return get_match(m)
    m = re.match(ident_pat, cur)
    if m: return get_match(m)
    m = re.match(r'[ \t]*(\"[^\"]*\")', cur)
    if m: return get_match(m)
    s = cur
    cur = ""
    return s

def getData():
    global cur
    ident_pat = r'^[a-zA-Z<>\-\+\*/_\.!\?=]' + \
                 r'[a-zA-Z<>\-\+\*/_\.!\?=0-9]*$'
    t = getToken()
    if t == '(':
        n = getToken()
        if n == ')':
            return None
        else:
            cur = n + cur
            flist = mlist = [getData(), 0]
            while True:
                n = getToken()
                if n == '.':
                    mlist[1] = getData()
                    if getToken() == ')':
                        break
                    else:
                        print("Parse Error!")
                elif n == ')':
                    mlist[1] = None
                    break
                else:
                    cur = n + cur
                    mlist[1] = [getData(), 0]
                    mlist = mlist[1]
            return flist
    elif t == "'":
        return ["quote", [getData(), None]]
    elif t == "`":
        return ["quasiquote", [getData(), None]]
    elif t == ",":
        return ["unquote", [getData(), None]]
    elif t == ",@":
        return ["unquote-splicing", [getData(), None]]
    elif t == "#t":
        return True
    elif t == "#f":
        return False
    elif re.match(r'^\-?[0-9]+/[0-9]+$', t):
        return Fraction(t)
    elif re.match(r'^\-?[0-9]+$', t):
        return int(t)
    elif re.match(r'^\-?[0-9]+\.[0-9]*([eE][+-]?[0-9]+)?$', t):
        return float(t)
    elif re.match(ident_pat, t):
        return t
    elif re.match(r'\"[^\"]*\"', t):
        return "str:" + t
    
def Data2Str(dat):
    if isinstance(dat, bool):
        if dat == True: return "#t"
        elif dat == False: return "#f"
    elif isinstance(dat, (int, float)): return repr(dat)
    elif isinstance(dat, Fraction):
        return repr(dat.numerator) + '/' + repr(dat.denominator)
    elif isinstance(dat, str):
        if dat.startswith("str:"): return dat[4:]
        else: return dat
    elif isinstance(dat, list):
        ss = "(" + Data2Str(dat[0])
        while True:
            if dat[1] == None:
                return ss + ")"
            elif not isinstance(dat[1], list):
                return ss + " . " + Data2Str(dat[1]) + ")"
            else:
                dat = dat[1]
                ss = ss + " " + Data2Str(dat[0])
    elif isinstance(dat, Procedure):
        return "<procedure %s>" % Data2Str(dat.param)
    elif isinstance(dat, Macro):
        return "<macro %s>" % Data2Str(dat.param)
    elif isinstance(dat, Promise):
        return "<promise>"
    elif dat == None:
        return "()"
    else:
        return repr(dat)

class Env():
    def __init__(self, parent):
        self.vars = []
        self.values = []
        self.parent = parent
    def add_binding(self, var, val):
        self.vars.append(var)
        self.values.append(val)
    def search(self, var):
        ## print "search string[%s]" % var
        for i, v in enumerate(self.vars):
            if v == var: return self.values[i]
        if self.parent != None:
            return self.parent.search(var)
        else:
            print("variable undefined: %s" % var)
            return None
    def set_var(self, var, val):
        for i, v in enumerate(self.vars):
            if v == var:
                self.values[i] = val
                return
        if self.parent != None:
            self.parent.set_var(var, val)
        else:
            print("cannot find variable: %s" % var)
    def find(self, var):
        for i, v in enumerate(self.vars):
            if v == var: return True
        return False

class Procedure():
    def __init__(self, param, body, env):
        self.param = param
        self.body = body
        self.env = env

class Macro():
    def __init__(self, param, body):
        self.param = param
        self.body = body

class Promise():
    def __init__(self, exp, env):
        self.exp = exp
        self.env = env

def car(lst): return lst[0]
def cdr(lst): return lst[1]
def caar(lst): return lst[0][0]
def cadr(lst): return lst[1][0]
def cdar(lst): return lst[0][1]
def cddr(lst): return lst[1][1]
def caddr(lst): return lst[1][1][0]

def make_list(*args):
    ss = None
    for x in reversed(args):
        ss = [x, ss]
    return ss

def eval_list(lst, env):
    if lst == None: return None
    else:
        return [eval(car(lst), env), eval_list(cdr(lst), env)]

def quote_list(lst):
    if lst == None: return None
    else:
        return [["quote", [car(lst), None]], quote_list(cdr(lst))]
        

def pylist(lst, env):
    ll = []
    while lst != None:
        ll.append(eval(car(lst), env))
        lst = cdr(lst)
    return ll

def list_len(lst):
    n = 0;
    while lst != None:
        n += 1
        lst = cdr(lst)
    return n

def operate_qq(exp, env):
    def splicing(head, tail):
        if head == None: return tail
        else:
            return [car(head), splicing(cdr(head), tail)]
    if isinstance(exp, list):
        if car(exp) == "unquote":
            return eval(cadr(exp), env)
        if isinstance(car(exp), list) and caar(exp) == "unquote-splicing":
            ll = eval(car(cdar(exp)), env)
            # print "-->", Data2Str(ll)
            return splicing(ll, operate_qq(cdr(exp), env))
        else:
            return [operate_qq(car(exp), env), operate_qq(cdr(exp), env)]
    else:
        return exp

def eval(exp, env):
    while True:
        if isinstance(exp, (int, float, Fraction)): return exp
        elif exp == False or exp == True: return exp
        elif isinstance(exp, str):
            if exp.startswith("str:"): return exp
            else: return env.search(exp)
        elif isinstance(exp, Promise):
            exp = Promise.exp
            env = Promise.env
            continue;
        elif isinstance(exp, list):
            if car(exp) == "quote":
                return cadr(exp)
            if car(exp) == "quasiquote":
                # print "-->", Data2Str(cadr(exp))
                res = operate_qq(cadr(exp), env)
                # print "<--", Data2Str(res)
                return res
            elif car(exp) == "set!":
                env.set_var(cadr(exp), eval(caddr(exp), env))
                return None
            elif car(exp) == "define":
                if isinstance(cadr(exp), str) and \
                   (not cadr(exp).startswith("str:")):
                    vval = eval(caddr(exp), env)
                    if env.find(cadr(exp)):
                        env.set_var(cadr(exp), vval)
                    else:
                        env.add_binding(cadr(exp), vval)
                    return "definition OK!"
                elif isinstance(cadr(exp), list):
                    exp = make_list("define",
                                    car(cadr(exp)),
                                    ["lambda",
                                     [cdr(cadr(exp)),
                                      cddr(exp)]])
                    continue;
                else:
                    print("Unknown style of definition: ", end=' ')
                    return exp
            elif car(exp) == "define-macro":
                if isinstance(cadr(exp), list):
                    mname = car(cadr(exp))
                    mparam = cdr(cadr(exp))
                    mbody = cddr(exp)
                    if env.find(mname):
                        env.set_var(mname, Macro(mparam, mbody))
                    else:
                        env.add_binding(mname, Macro(mparam, mbody))
                    # print "macro definition OK!"
                    return "macro definition OK!"
                else:
                    print("wrong macro form!")
            elif car(exp) == "delay":
                return Promise(cadr(exp), env)
                
            elif car(exp) == "if":
                vv = eval(cadr(exp), env)
                if vv:
                    # return eval(caddr(exp), env)
                    exp = caddr(exp)
                    continue;
                else:
                    # return eval(cadr(cddr(exp)), env)
                    if cdr(cddr(exp)) == None: return None
                    exp = cadr(cddr(exp))
                    continue;
            elif car(exp) == "lambda":
                param = cadr(exp)
                body = cddr(exp)
                proc = Procedure(param, body, env)
                return proc
            elif car(exp) == "begin":
                if cdr(exp) == None: return None
                exp = cdr(exp)
                while cdr(exp) != None:
                    eval(car(exp), env)
                    exp = cdr(exp)
                # return eval(car(exp), env)
                exp = car(exp)
                continue
            elif car(exp) == "let":
                if isinstance(cadr(exp), str) and \
                   not cadr(exp).startswith("str:"):
                    exp = ["named-let", cdr(exp)]
                    # print Data2Str(exp)
                    continue
                vars = cadr(exp)
                body = cddr(exp)
                eenv = Env(env)
                while vars != None:
                    binding = car(vars)
                    eenv.add_binding(car(binding), eval(cadr(binding), env))
                    vars = cdr(vars)
                while cdr(body) != None:
                    eval(car(body), eenv)
                    body = cdr(body)
                exp = car(body)
                env = eenv
                continue
            elif car(exp) == "letrec":
                vars = cadr(exp)
                body = cddr(exp)
                eenv = Env(env)
                while vars != None:
                    binding = car(vars)
                    val = eval(cadr(binding), env)
                    if isinstance(val, Procedure): val.env = eenv
                    eenv.add_binding(car(binding), val)
                    vars = cdr(vars)
                while cdr(body) != None:
                    eval(car(body), eenv)
                    body = cdr(body)
                exp = car(body)
                env = eenv
                continue
            elif car(exp) == "let*":
                vars = cadr(exp)
                body = cddr(exp)
                while vars != None:
                    eenv = Env(env)
                    binding = car(vars)
                    eenv.add_binding(car(binding), eval(cadr(binding), env))
                    vars = cdr(vars)
                    env = eenv
                while cdr(body) != None:
                    eval(car(body), eenv)
                    body = cdr(body)
                exp = car(body)
                env = eenv
                continue
            elif car(exp) == "cond":
                clauses = cdr(exp)
                ## print Data2Str(clauses)
                while clauses != None:
                    mm = car(clauses)
                    if car(mm) == "else" or eval(car(mm), env): break
                    clauses = cdr(clauses)
                if clauses == None:
                    print("cond: no match syntax rules")
                    return None
                else:
                    ## print Data2Str(mm)
                    exp = ["begin", cdr(mm)]
                    ## print Data2Str(exp)
                    continue
            elif car(exp) == "and":
                m = cdr(exp)
                if m == None: return True
                while cdr(m) != None:
                    if eval(car(m), env) == False:
                        return False
                    m = cdr(m)
                exp = car(m)
                continue
            elif car(exp) == "or":
                m = cdr(exp)
                if m == None: return False
                while cdr(m) != None:
                    vv = eval(car(m), env)
                    if vv != False:
                        return vv
                    m = cdr(m)
                exp = car(m)
                continue
            elif car(exp) == "apply":
                func = eval(cadr(exp), env)
                args = eval(car(cddr(exp)), env)
                exp = [func, quote_list(args)]
                ## print "-->", Data2Str(exp)
                continue
            else:
                op = eval(car(exp), env)
                if isinstance(op, Procedure):
                    eenv = Env(op.env)
                    # args = pylist(cdr(exp), env)
                    args = cdr(exp)
                    param = op.param
                    body = op.body

                    while args != None:
                        if isinstance(param, str):
                            eenv.add_binding(param, eval_list(args, env))
                            param = None
                            break;
                        elif isinstance(param, list):
                            eenv.add_binding(car(param), eval(car(args), env))
                            param = cdr(param)
                            args = cdr(args)
                        else:
                            print("parameters do not match! (too many arguments)")
                            return None
                    if param != None:
                        print("parameters do not match! (too few arguments)")
                        return None
                    # if list_len(op.param) != len(args):
                    #     print "number of parameters are wrong!"
                    #     return None
                    # for i in range(len(args)):
                    #     eenv.add_binding(car(param), args[i])
                    #     param = cdr(param)
                    if body == None: return None
                    while cdr(body) != None:
                        eval(car(body), eenv)
                        body = cdr(body)
                    # return eval(car(body), eenv)
                    exp = car(body)
                    env = eenv
                    continue;
                if isinstance(op, Macro):
                    eenv = Env(env)
                    args = cdr(exp)
                    param = op.param
                    body = op.body

                    while args != None:
                        if isinstance(param, str):
                            eenv.add_binding(param, args)
                            param = None
                            break;
                        elif isinstance(param, list):
                            eenv.add_binding(car(param), car(args))
                            param = cdr(param)
                            args = cdr(args)
                        else:
                            print("parameters do not match! (too many arguments)")
                            return None
                    if param != None:
                        print("parameters do not match! (too few arguments)")
                        return None
                    if body == None: return None
                    # print Data2Str(car(body))
                    exp = eval(car(body), eenv)
                    # print Data2Str(exp)
                    continue;
                else:
                    try:
                        if isinstance(op, (types.FunctionType, \
                                           types.BuiltinFunctionType, \
                                           types.MethodType)):
                            val = op(*pylist(cdr(exp), env))
                        else:
                            print("operator is not callable: %s" % Data2Str(op))
                            return None
                    except TypeError:
                        print("probably wrong number of arguments.")
                        return None;
                    return val
        return exp

def plus_f(*args):
    s = 0
    for x in args:
        s += x
    if isinstance(s, Fraction) and s.denominator == 1:
        s = int(s)
    return s

def minus_f(*args):
    s = args[0]
    for x in args[1:]:
        s = s - x
    if isinstance(s, Fraction) and s.denominator == 1:
        s = int(s)
    return s

def times_f(*args):
    p = 1
    for x in args:
        p *= x
    if isinstance(p, Fraction) and p.denominator == 1:
        p = int(p)
    return p

def div_f(*args):
    p = args[0]
    for x in args[1:]:
        if isinstance(p, int) and isinstance(x, int):
            p = Fraction(p, x)
        else:
            p = p / x
    if isinstance(p, Fraction) and p.denominator == 1:
        p = int(p)
    return p

def remainder_f(x, y):
    return x % y

def abs_f(x):
    return abs(x)

def min_f(*arg):
    return min(arg)

def max_f(*arg):
    return max(arg)

def car_f(lst): return lst[0]
def cdr_f(lst): return lst[1]
def cons_f(x, y): return [x, y]
def is_pair(lst): return (isinstance(lst, list) and len(lst) == 2)
def set_car(lst, x):
    lst[0] = x
    return lst
def set_cdr(lst, x):
    lst[1] = x
    return lst
def nullp_f(x): return (x == None)
def eqp_f(x, y): return (id(x) == id(y))
def zerop_f(x): return (x == 0)
def equalp_f(x, y): return (x == y)
def gtp_f(x, y): return (x > y)
def gep_f(x, y): return (x >= y)
def ltp_f(x, y): return (x < y)
def lep_f(x, y): return (x <= y)
def display_f(s):
    if isinstance(s, str) and s.startswith("str:"): ss = s[5:-1]
    else: ss = Data2Str(s)
    sys.stdout.write(ss)
    return None
def newline_f():
    print()
    return None

def string_equalp(str1, str2):
    return isinstance(str1, str) and str1.startswith("str:") and \
           isinstance(str2, str) and str2.startswith("str:") and \
           str1 == str2

def string_append_f(*args):
    s = 'str:"'
    for x in args:
        s = s + x[5:-1]
    return s + '"'
def string_upcase_f(str):
    return 'str:"' + str[5:-1].upper() + '"'
def string_downcase_f(str):
    return 'str:"' + str[5:-1].lower() + '"'
def number2string_f(n):
    return 'str:"' + repr(n) + '"'
def substring_f(s, start, end):
    ss = s[5:-1]
    return 'str:"' + ss[start:end] + '"'

def numberp_f(x):
    return isinstance(x, int) or isinstance(x, float) \
        or isinstance(x, Fraction)
def eq_numberp_f(x, y):
    return numberp_f(x) and numberp_f(y) and (x == y)

def str_len_f(s):
    return len(s[5:-1])

def quotient_f(x, y):
    return x // y

def load_f(fname):
    global filept, mode, the_global_env
    oldfp = filept
    oldmode = mode
    fname = fname[5:-1]
    try:
        filept = open(fname, 'r')
    except IOError:
        print("file not found: %s" % fname)
        return
    mode = "file"
    try:
        while True:
            s = getData()
            eval(s, the_global_env)
    except EOFError:
        if mode == "interactive":
            print("read file: " + fname)
    except KeyboardInterrupt:
        pass
    filept.close()
    filept = oldfp
    mode = oldmode

def exit_f():
    raise EOFError()

def force_f(d):
    if isinstance(d, Promise):
        return eval(d.exp, d.env)
    else:
        return d

def pyeval_f(name):
    return eval_orig(name)

def make_graphics(draw_func):
    xsize, ysize = 400, 400
    root = Tk()
    root.title("Scheme on the top of Python")
    root.geometry("%dx%d" % (xsize, ysize))
    c0 = Canvas(root, width = xsize, height = ysize)
    c0.pack()
    def line_func(x1, y1, x2, y2):
        # print "line_func", x1, y2, x2, y2
        c0.create_line(x1 * xsize , y1 * ysize, x2 * xsize, y2 * ysize);
    def box_func(x1, y1, x2, y2):
        c0.create_rectangle(x1 * xsize , y1 * ysize,
                            x2 * xsize, y2 * ysize,
                            fill="black", outline=None);
    eval([draw_func, [line_func, [box_func, None]]], Env(None))
    root.mainloop()

def init_env():
    global_env = Env(None)
    funcs = ["+", plus_f, "-", minus_f, "*", times_f, "/", div_f,
             "car", car_f, "cdr", cdr_f, "cons", cons_f, "null?", nullp_f,
             "pair?", is_pair, "set-car!", set_car, "set-cdr!", set_cdr,
             "zero?", zerop_f, "eq?", eqp_f, ">", gtp_f, ">=", gep_f,
             "<", ltp_f, "<=", lep_f, "display", display_f,
             "newline", newline_f, "remainder", remainder_f,
             "abs", abs_f, "min", min_f, "max", max_f,
             "string=?", string_equalp,
             "string-append", string_append_f,
             "string-upcase", string_upcase_f,
             "string-downcase", string_downcase_f,
             "substring", substring_f,
             "number->string", number2string_f,
             "=", eq_numberp_f, "string-length", str_len_f,
             "number?", numberp_f,
             "quotient", quotient_f, "sqrt", math.sqrt,
             "sin", math.sin, "cos", math.cos, "tan", math.tan,
             "floor", math.floor, "ceiling", math.ceil,
             "inexact->exact", lambda x: int(x),
             "load", load_f,
             "exit", exit_f, "force", force_f, "pyeval", pyeval_f,
             "make-graphics", make_graphics]
    for i in range(len(funcs) // 2):
        global_env.add_binding(funcs[i * 2], funcs[i * 2 + 1])
    return global_env

init_str = """\
(define (cadr x) (car (cdr x)))
(define (caar x) (car (car x)))
(define (cddr x) (cdr (cdr x)))
(define (cdar x) (cdr (car x)))
(define (caddr x) (car (cdr (cdr x))))
(define (cadddr x) (car (cdr (cddr x))))
(define (caadr x) (car (car (cdr x))))
(define (sqr x) (* x x))
(define (even? x) (= (remainder x 2) 0))
(define (odd? x) (= (remainder x 2) 1))
(define (clock) ((pyeval 'time.clock)))
(define (random) ((pyeval 'random.random)))
(define (map f lst)
  (if (null? lst) '()
      (cons (f (car lst)) (map f (cdr lst)))))
(define (append lst1 lst2)
  (if (null? lst1) lst2
      (cons (car lst1) (append (cdr lst1) lst2))))
(define (reverse lst)
  (define (rev-iter lst lstr)
    (if (null? lst) lstr
	(rev-iter (cdr lst) (cons (car lst) lstr))))
  (rev-iter lst '()))
(define (list . args) args)
(define (list? lst)
  (if (null? lst) #t
      (if (pair? lst) (list? (cdr lst)) #f)))
(define (length lst)
  (define (length-iter lst n)
     (if (null? lst) n
         (length-iter (cdr lst) (+ n 1))))
  (length-iter lst 0))
(define (not x) (if x #f #t))
(define-macro (when cond . body)
  `(if ,cond 
      (begin . ,body)))
(define-macro (named-let name bindings . body)
  `(letrec ((,name (lambda ,(map car bindings) ,@body)))
     (,name . ,(map cadr bindings))))
"""

def init_definition(global_env):
    global filept, mode
    oldfp = filept
    oldmode = mode
    filept = io.StringIO(init_str)
    mode = "file"
    try:
        while True:
            s = getData()
            eval(s, global_env)
    except EOFError:
        if oldmode == "interactive":
            print("Initialization Complete.")
    filept = oldfp
    mode = oldmode

def main():
    global filept, mode, the_global_env
    greeting = "Welcome to Scheme on Python." + \
               "  Copyright (C) 2018, 2019 Osami Yamamoto"
    ## print  sys.argv
    if len(sys.argv) == 2:
        filept = open(sys.argv[1], "r")
        mode = "file"
    if mode == "interactive":
        print(greeting)
        print("Python " + sys.version)
    the_global_env = global_env = init_env()
    init_definition(global_env)
    try:
        while True:
            try:
                res = ""
                s = getData()
                res = Data2Str(eval(s, global_env))
            except KeyboardInterrupt:
                print("Keyboard Interrupt!")
            else:
                if mode == "interactive": print(res)
    except EOFError:
        if mode == "interactive":
            print()
            print("Bye! See you next time.")

if __name__ == '__main__':
    main()

#

import sys
import os.path

dir = os.path.dirname(__file__) + os.path.sep
if sys.version_info.major == 3:
	exec(compile(open(dir + "Python3/main.py", "rb").read(),
		dir + "Python3/main.py", 'exec'))
else:
	execfile(dir + "main.py")


(letrec ((fib (lambda (n)
		(if (<= n 1) 1
		    (+ (fib (- n 1)) (fib (- n 2)))))))
  (let ((res (fib 30)))
    (display res)
    (newline)))

  


(display "start")
(newline)

(let loop ((i 0))
  (if (< i 10)
      (begin
	(display i)
	(loop (+ i 1)))))
(newline)

(named-let loop ((i 0))
	   (if (< i 10)
	       (begin
		 (display i)
		 (loop (+ i 1)))))
	    
(newline)

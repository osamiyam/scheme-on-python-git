;; (define (sqr x) (* x x))

(define (make-integer i)
  (cons i (delay (make-integer (+ i 1)))))
(define integer (make-integer 0))

(define (take lst n)
  (if (zero? n) '()
      (cons (car lst) (take (force (cdr lst)) (- n 1)))))

(define (stream-sqr lst)
  (cons (sqr (car lst)) (delay (stream-sqr (force (cdr lst))))))

(define (stream-filter condf lst)
  (if (condf (car lst))
      (cons (car lst) (delay (stream-filter condf (force (cdr lst)))))
      (stream-filter condf (force (cdr lst)))))

(let ((m (stream-sqr integer))
      (n 10))
  (display (take integer n))
  (newline)
  (display (take m n))
  (newline))

(define (even? n) (zero? (remainder n 2)))
(display (take (stream-filter even? integer) 10))
(newline)

(define (prime? n)
  (define (prime?-iter i)
    (cond ((> (* i i) n) #t)
	  ((zero? (remainder n i)) #f)
	  (else (prime?-iter (+ i 1)))))
  (prime?-iter 2))

(display (prime? 13))
(newline)

(define primes (stream-filter prime? (force (cdr (force (cdr integer))))))
(display (take primes 20))
(newline)

(define (stream-nth lst n)
  (if (zero? n) (car lst)
      (stream-nth (force (cdr lst)) (- n 1))))

(display (stream-nth primes 1000))
(newline)


  





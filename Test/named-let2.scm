
(define (kuku n)
  (let loop1 ((i 0))
    (when (< i n)
        (let loop2 ((j 0))
          (when (< j n)
             (let ((k (* i j)))
               (display (if (< k 10) "  " " "))
               (display "  ")
               (display k)
               (loop2 (+ j 1)))))
        (newline)
        (loop1 (+ i 1)))))
(kuku 10)

          
  

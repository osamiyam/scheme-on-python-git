

(define (foo)
  (define (iter i)
    (if (zero? i) '()
	(iter (- i 1))))
  (iter 100000)
  (display ".")
  (newline)
  (foo))

(foo)

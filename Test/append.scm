
(define (append lst1 lst2)
  (if (null? lst1) lst2
      (cons (car lst1) (append (cdr lst1) lst2))))

(define s '(a b c))
(define t '(x y z))
(display (append s t))
(newline)

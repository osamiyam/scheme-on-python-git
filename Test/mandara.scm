;;--------------------------------------------------
;; Title: Mandara on Scheme on the top of Python
;; Author: Osami Yamamoto
;; Date: Sat Mar 16 18:06:53 JST 2019
;;--------------------------------------------------

(define (myfor i n func)
  (when (< i n)
	(func i)
	(myfor (+ i 1) n func)))

(define (nth i lst)
  (if (zero? i) (car lst)
      (nth (- i 1) (cdr lst))))

(define N 21)
(define pi 3.141592)
(define r 0.45)
(define (make_data i)
  (if (zero? i) '()
      (let ((th (/ (* 2.0 pi i) N)))
	(let ((x (+ (* r (cos th)) 0.5))
	      (y (+ (* r (sin th)) 0.5)))
	  (cons (cons x y) (make_data (- i 1)))))))
(define data (make_data N))

(make-graphics
 (lambda (line box)
   (myfor 0 N
	  (lambda (i)
	    (myfor (+ i 1) N
		   (lambda (j)
		     (let ((pt1 (nth i data))
			   (pt2 (nth j data)))
		       (line (car pt1) (cdr pt1)
			     (car pt2) (cdr pt2)))))))))


	  
 

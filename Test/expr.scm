
(define (expr a n)
  (if (zero? n) 1
      (let ((v (expr a (quotient n 2))))
	(if (zero? (remainder n 2))
	    (* v v)
	    (* v v a)))))

(display (expr 2 1000))
(newline)


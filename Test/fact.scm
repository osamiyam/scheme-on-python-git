
(define (fact n)
  (define (fact0 i n res)
    (if (> i n) res
	(fact0 (+ i 1) n (* res i))))
  (fact0 1 n 1))

(display (fact 30))
(newline)

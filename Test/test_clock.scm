
(define-macro (work prog)
  `(let ((t1 (clock)))
     (let ((res ,prog))
       (let ((t2 (clock)))
	 (display (list (cons 'result res)
			(cons 'time (- t2 t1))))
	 (newline)))))

(define (fib n)
  (if (<= n 1) 1
      (+ (fib (- n 1)) (fib (- n 2)))))

(work (fib 25))



(define (padd p1 p2)
  (let ((p1x (car p1)) (p1y (cdr p1))
	(p2x (car p2)) (p2y (cdr p2)))
    (cons (+ p1x p2x) (+ p1y p2y))))

(define (carpet p1 p2 depth box)
  (if (zero? depth) (box (car p1) (cdr p1) (car p2) (cdr p2))
      (let ((width (/ (- (car p2) (car p1)) 3))
	    (depth1 (- depth 1)))
	(let ((dp (cons width width))
	      (dpx (cons width 0.0))
	      (dpy (cons 0.0 width)))
	  (carpet p1 (padd p1 dp) depth1 box)
	  (carpet (padd p1 dpx) (padd (padd p1 dpx) dp) depth1 box)
	  (carpet (padd (padd p1 dpx) dpx)
	   	  (padd (padd (padd p1 dpx) dpx) dp) depth1 box)
	  
	  (carpet (padd p1 dpy) (padd (padd p1 dpy) dp) depth1 box)
	  (carpet (padd (padd p1 dp) dpx) (padd (padd (padd p1 dp) dpx) dp)
	   	  depth1 box)
	  
	  (carpet (padd (padd p1 dpy) dpy) (padd (padd (padd p1 dpy) dpy) dp)
	   	  depth1 box)
	  (carpet (padd (padd p1 dpy) dp) (padd (padd (padd p1 dpy) dp) dp)
	   	  depth1 box)
	  (carpet (padd (padd p1 dp) dp) (padd (padd (padd p1 dp) dp) dp)
	   	  depth1 box)))))

(make-graphics
 (lambda (line box)
   (carpet (cons 0.0 0.0) (cons 1.0 1.0) 4 box)))

	  
	  
	

;;=================================================
;; Title: kuku table
;; Author: Osami Yamamoto
;; Date: Mon Dec 10 09:22:07 JST 2018
;;=================================================

(define (myfor i n func)
  (if (>= i n) '()
      (begin
	(func i)
	(myfor (+ i 1) n func))))

(define (kuku n)
  (myfor 0 n
	 (lambda (i)
	   (myfor 0 n
		  (lambda (j)
		    (display (if (< (* i j) 10) "   " "  "))
		    (display (* i j))))
	   (newline))))
(kuku 10)

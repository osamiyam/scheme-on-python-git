
(define (sum n)
  (if (zero? n) 0
      (+ n (sum (- n 1)))))

(display (sum 100))
(newline)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Generation of Permutations
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (filter cnd lst)
  (cond ((null? lst) '())
        ((cnd (car lst))
         (cons (car lst) (filter cnd (cdr lst))))
        (else (filter cnd (cdr lst)))))

(define (map func lst)
  (if (null? lst) '()
      (cons (func (car lst)) (map func (cdr lst)))))

(define (append-all lst)
  (if (null? lst) '()
      (append (car lst) (append-all (cdr lst)))))

(define (perm lst)
  (if (null? lst) '(())
      (append-all
       (map (lambda (x)
	      (let ((y (perm (filter (lambda (m)
				       (not (eq? x m))) lst))))
		(map (lambda (m) (cons x m)) y))) lst))))

(display (perm '(1 2 3 4 5)))(newline)

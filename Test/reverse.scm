
(define (reverse lst)
  (define (reverse-iter lst res)
    (if (null? lst) res
	(let ((head (car lst))
	      (tail (cdr lst)))
	  (reverse-iter tail (cons head res)))))
  (reverse-iter lst '()))

(define (test)
  (let ((lst '(1 2 3 4 5)))
    (display (reverse lst))
    (display (reverse (reverse lst)))
    (newline)))
(test)



  

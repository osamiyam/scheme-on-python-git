
(define (fb n)
  (letrec ((judge (lambda (i d) (zero? (remainder i d))))
	   (fb-iter (lambda (i n)
		      (if (eq? i n) '()
			  (begin
			    (display 
			     (cond
			      ((judge i 15) "FizzBuzz, ")
			      ((judge i 3) "Fizz, ")
			      ((judge i 5) "Buzz, ")
			      (else (string-append (number->string i) ", "))))
			    (fb-iter (+ i 1) n))))))
    (fb-iter 1 n)
    (newline)))
  
(fb 60)

	

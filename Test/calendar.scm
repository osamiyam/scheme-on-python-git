
(define (nth n lst)
  (if (= n 0) (car lst)
      (nth (- n 1) (cdr lst))))


(define (leapyear? year)
  (define (check n) (= (remainder year n) 0))
  (if (check 400) #t
      (if (check 100) #f
          (if (check 4) #t #f))))

(define (days-from-year-one year)
  (define (days-from-year-one-iter y year res)
    (if (= y year) res
        (days-from-year-one-iter
	 (+ y 1)
	 year
	 (+ res (if (leapyear? y) 366 365)))))
  (days-from-year-one-iter 1 year 0))

(define nmonth '(31 28 31 30 31 30 31 31 30 31 30 31))
(define monthname '("January" "February" "March" "April" "May"
                    "June" "July" "August" "September" "October"
                    "November" "December"))
(define weekname " Su Mo Tu We Th Fr Sa")

(define (total-days month year)
  (define (total-days-iter m month total)
    (if (= m month) total
        (total-days-iter
         (+ m 1) month
         (+ total (nth (- m 1) nmonth)
            (if (and (= m 2) (leapyear? year)) 1 0)))))
  (total-days-iter 1 month (days-from-year-one year)))

(define (day-of-week month year)
  (remainder (+ 1 (total-days month year)) 7))

(define (int->string n)
  (if (< n 10) (string-append "  " (number->string n))
      (string-append " " (number->string n))))

(define (spaces n)
  (if (= n 0) ""
      (string-append " " (spaces (- n 1)))))

(define (prcal month year)
  (define (print-title)
    (let ((ss (string-append (nth (- month 1) monthname) " "
                             (int->string year))))
      (let ((ll (string-length ss)))
        (display (spaces (quotient (- 21 ll) 2)))
        (display ss)
        (newline))))
  (define (prcal-iter d md i)
    (if (> d md) '()
        (let ((ii (remainder (+ i 1) 7))) 
          (display (int->string d))
          (if (=  ii 0) (newline))
          (prcal-iter (+ d 1) md ii))))
  (print-title)
  (display weekname)
  (newline)
  (let ((md (+ (nth (- month 1) nmonth)
               (if (and (= month 2) (leapyear? year)) 1 0)))
        (dw (day-of-week month year)))
    (display (spaces (* dw 3)))
    (prcal-iter 1 md dw)
    (newline)))

;;(prcal 4 2015)
(prcal 10 2015)
;; (prcal 9 1961)
;;(prcal 2 2004)


(display (day-of-week 12 2015))
(newline)

(prcal 12 2018)
(newline)
(prcal 1 2019)

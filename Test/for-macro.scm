
(define (myfor i n func)
  (if (< i n)
      (begin
	(func i)
	(myfor (+ i 1) n func))
      '()))

;; (myfor 0 10 (lambda (i) (display i) (newline)))


(define-macro (for var i n . func)
  `(myfor ,i ,n (lambda (,var) . ,func)))

;; (for i 0 10 (display i) (newline))

(define (test)
  (for i 0 10
       (for j 0 10
	    (let ((k (* i j)))
	      (display (if (< k 10) "   " "  "))
	      (display k)))
       (newline)))
(test)

	    
